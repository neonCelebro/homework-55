import React from 'react';
import './GameSquearComponent.css';

const GameSquearComponent = (props) => {
  return (
    <div className='boxesConteiner'>
        {props.states.map((item, index) => <div
           key={index}
           onClick={props.removeBox}
           className={item.isClass.join(' ')}>
           </div>)}
    </div>
  )
}


export default GameSquearComponent;
