import React from 'react';
import './ButtonReset.css';

const Button = (props) =>
    <button className="button" onClick={props.reset}>Reset</button>


export default Button;
