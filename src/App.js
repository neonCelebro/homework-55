import React, { Component } from 'react';
import './App.css';
import Button from './components/ButtonComponent/ButtonReset';
import GameSquearComponent from './components/GameSquearComponent/GameSquearComponent.js';


class App extends Component {
    state = {
      boxes: [],
      count: 0,
      endGame: false
    };

itemsArray = () => {
  const boxes = [];
  const box = {isClass : ['box']}
  while (boxes.length !== 36) {
    boxes.push(box)
  };
  this.setState({boxes});
  setTimeout(() => {
    this.addRingInRandomBox();
  }, 100);
};

addRingInRandomBox = () => {
  let i = Math.floor(Math.random() * (36 - 0) + 0);
    let boxes = this.state.boxes.map((item, index) => {
      if (index === i) {
        item.heveARing = true;
        item.isClass.push('boxWithRing');
        return item;
      }else {
        return {isClass : ['box']};
      }
    });
    this.setState({boxes});
  };
  removeBox = (id) => {
    if (this.state.endGame === false) {
      const index = id.target.classList;
      for (var i = 0; i < index.length; i++) {
        if (index[i] !== 'boxWithRing') {
          index.value = index.value + ' hide';
        }if ((index[i] !== 'hide')) {
          setTimeout(() => {
            this.setState({count: this.state.count + 1});
          }, 100);
        } if (index[i] === 'boxWithRing') {
          setTimeout(() => {
            alert('Конец игры!');
            const endGame = true;
              this.setState({endGame});
          }, 300);
        }
      }
    }
    };
resetGame = () => {
  const boxes = [];
  const endGame = false;
  const count = 0;
  this.setState({boxes, endGame, count});
  setTimeout(() => {
    this.itemsArray();
  }, 100);
}
componentDidMount(){
  this.itemsArray();
}
  render() {
    return (
      <div className="App">
      <Button reset={this.resetGame}></Button>
      <GameSquearComponent
      removeBox={this.removeBox}
      states={this.state.boxes}/>
      <h2 className="counts">Tries: {this.state.count}</h2>

      </div>
    );
  }
}

export default App;
